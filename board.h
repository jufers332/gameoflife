#ifndef BOARD_H
#define BOARD_H

#include <stdio.h>
#include <stdint.h>
#include <ctype.h>
#include <time.h>
#include <string.h>
#include <math.h>
#include <assert.h>
#include <stdlib.h>

#include "SDL.h"

#define FALSE 0
#define TRUE 1

#define LIVING_CELL_R 255
#define LIVING_CELL_G 255
#define LIVING_CELL_B 255
#define DEAD_CELL_R 20
#define DEAD_CELL_G 20
#define DEAD_CELL_B 20

typedef Uint8 bool;

typedef struct
{
    int rows;
    int columns;
    bool grid[ 0 ];
} board;

typedef struct
{
    int camera_x;
    int camera_y;
    int cell_size;
    int height_in_cells;
    int width_in_cells;
    int window_height;
    int window_width;
    int movement_speed_in_cells;
    int min_movement_speed_in_pixels;
} view;
board* init_board( int rows, int columns, int living_cell_count );
int update_board( board* b );
bool cell_state( int x, int y, board* b );
bool updated_cell_state( int x, int y, board* b );
void toggle_cell_state( int x, int y, board *b );
void draw_board( board* b, view *player_view, SDL_Renderer* renderer );
void kill_all_cells( board* b );
void resize_board_view( int zoom, view* player_view, board* world );
void move_camera_by( int x, int y, view* player_view, board* game_board, SDL_Window* window );

#endif
